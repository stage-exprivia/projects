# Changelog

## [1.0.0] - 14/04/2022

### Added
- Exception
- GlobalExceptionHandler
- Models(Employee, Project, Relation)
- BatchControllerInterface, ControllerInterface
- Repositories(EmployeeRepository, ProjectRepository, RelationRepository)
- BatchServiceInterface, ServiceInterface

### Changed

- 

### Removed

- 

### Fixed

- 
