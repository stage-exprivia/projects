package com.exprivia.project.main.service;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.exprivia.commonlibrary.batch.BatchReport;
import com.exprivia.commonlibrary.exceptions.ResourceNotFoundException;
import com.exprivia.commonlibrary.filter.QueryBuilder;
import com.exprivia.commonlibrary.models.Project;
import com.exprivia.commonlibrary.repository.ProjectRepository;
import com.exprivia.project.main.batch.Batch;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.mock.web.MockMultipartFile;

@ExtendWith(MockitoExtension.class)
public class ServiceTest {
    @Mock
    private ProjectRepository projectRepository;

    @Mock
    private MongoTemplate mongoTemplate;

    @Mock
    private QueryBuilder queryBuilder;

    @InjectMocks
    private ProjectService service;

    @InjectMocks
    private Batch readFile;

    @Test
    public void insertExcelTest() throws FileNotFoundException, IOException {
        MockMultipartFile excel = new MockMultipartFile("risorseTest", new FileInputStream(new File("D:/Users/lessi/Desktop/Progetti lavoro/ProjectsGit/src/main/resources/dataTreCampi.xlsx")));
        BatchReport batchReport = readFile.readExcel(excel);
        List<Project> project = batchReport.getValidResources();

        when(projectRepository.saveAll(any())).thenReturn(project);

        BatchReport expected = service.insertResourcesFromExcel(excel);
        System.out.println(batchReport);
        assertEquals(batchReport.getValidResources(), expected.getValidResources());
        assertEquals(batchReport.getNotValidResources(), expected.getNotValidResources());
        assertEquals(batchReport.getReport(), expected.getReport());
        verify(projectRepository, times(1)).saveAll(project);
    }
    
    @Test
    public void getProject() {
        List<Project> projects = new ArrayList<>();
        projects.add(new Project("dsnjnsdf","jfidkjfd","fnkdcf"));
        projects.add(new Project("dsnjnsdf","jfidkjfd","fnkdcf"));
        projects.add(new Project("dsnjnsdf","jfidkjfd","fnkdcf"));

        Pageable paging = PageRequest.of(0, 3, Sort.by("accountManager"));
        Page<Project> pagedTasks = new PageImpl(projects);

        when(projectRepository.findAll(paging)).thenReturn(pagedTasks);
        List<Project> expected = service.getAllResources(0,3,"accountManager","ASC");

        Assertions.assertEquals(expected, projects);
        verify(projectRepository).findAll(paging);
    }
@Test
public void getprojectbyId() throws  ResourceNotFoundException{
    Project project = new Project();
    project.setId("1L");
    project.setAccountManager("name");
    project.setPmdm("surname");

    when(projectRepository.findById(project.getId())).thenReturn(Optional.of(project));
    Project expected = service.getResourceById(project.getId());
    assertEquals(expected, project);

}       

@Test
public void getProjectWithQueryTestResult()throws ResourceNotFoundException, IllegalArgumentException, IllegalAccessException, ResourceNotFoundException{

    Project prj = new Project("dsnjnf","jfidkjfd","fnkdcf");
    
    Pageable paging = PageRequest.of(0, 10, Sort.by("accountManager"));

    String queryParam="id:dsnjnf;";
    Query query =new Query();
    when(queryBuilder.buildQuery(queryParam, Project.class)).thenReturn(query);
    List<Project> queryProjects = new ArrayList<>();
    queryProjects.add(prj);
   
    when(mongoTemplate.find(query, Project.class)).thenReturn(queryProjects);
    System.out.println(queryProjects);

    List<Project> expected = service.getQueriedResources(0,10,"accountManager","asc","id:dsnjnf;");

    Assertions.assertEquals(expected, queryProjects);
    verify(mongoTemplate).find(query, Project.class);

}

    @Test
    public void createproject() throws Exception{
        Project project = new Project();
        project.setId("Test Id");

        when(projectRepository.save(ArgumentMatchers.any(Project.class))).thenReturn(project);

        Project newProject = service.createResource(project);

        Assertions.assertEquals(newProject.getId(), project.getId());
        verify(projectRepository).save(project);
    }
    @Test
    public void deleteproject() throws ResourceNotFoundException, ResourceNotFoundException{
        Project project = new Project();
        project.setAccountManager("Test Name");
        project.setId("1L");
        
        when(projectRepository.findById(project.getId())).thenReturn(Optional.of(project));
        service.deleteResource(project.getId());
        verify(projectRepository).deleteById(project.getId());
    }

    @Test
    public void getByIdException() {
       when(projectRepository.findById(anyString())).thenReturn(Optional.empty());
       ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class, () -> {service.getResourceById("1");});
       assertEquals("Resource not found with id " + 1 + ".", exception.getMessage());

    }
    @Test
    public void deleteException() {
       when(projectRepository.findById(anyString())).thenReturn(Optional.empty());
       ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class, () -> {service.deleteResource("1");});
       assertEquals("Resource not found with id " + 1 + ".", exception.getMessage());
    }
}
/*


        

    








/*
    @Test
    public void getByIdException() {
       when(projectRepository.findById(anyString())).thenReturn(Optional.empty());
       ProjectNotFound exception = assertThrows(ProjectNotFound.class, () -> {service.getProjectbyId("gggg");});
       assertEquals("Project not found with id " + "gggg" + ".", exception.getMessage());
    }




    @Test
    public void deleteException() {
       when(projectRepository.findById(anyString())).thenReturn(Optional.empty());
       ProjectNotFound exception = assertThrows(ProjectNotFound.class, () -> {service.deleteProject("nvjjnd");;});
       assertEquals("Project not found with id " + "nvjjnd" + ".", exception.getMessage());
    }

    @Test
    public void updateprojectTest() throws ProjectNotFound{
        Project project = new Project();
        project.setId("89L");
        project.setName("Test Name");

        Project newProject = new Project();
        newProject.setId("89L");
        newProject.setName("New Test Name");

        when(projectRepository.findById(project.getId())).thenReturn(Optional.of(project));
        service.updateProject(newProject, newProject.getId());

        verify(projectRepository).save(newProject);
        verify(projectRepository).findById(project.getId());    
    }

    @Test
    public void updateException() {
        Project project = new Project();
        project.setName("Test");
       when(projectRepository.findById(project.getId())).thenReturn(Optional.empty());
       ProjectNotFound exception = assertThrows(ProjectNotFound.class, () -> {service.updateProject(project, "1L");});
       assertEquals("Project not found with id " + "1L" + ".", exception.getMessage());
    }
    */

