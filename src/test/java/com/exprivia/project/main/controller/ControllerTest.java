package com.exprivia.project.main.controller;


import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.exprivia.commonlibrary.models.Project;
import com.exprivia.project.main.service.ProjectService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.multipart.MultipartFile;

@ContextConfiguration(classes={SpringBootApplication.class})
@Import(ProjectController.class)
@ExtendWith(SpringExtension.class)
@WebMvcTest(ProjectController.class)
@CrossOrigin("http://localhost:8080")


public class ControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ProjectService service;

    @Test
    public void insertExcelTest() throws Exception{
        MockMultipartFile file=new MockMultipartFile("file","D:/projects/lessi/Desktop/Progetti lavoro/Progetto/src/test/resources/dataTestCentoCampi.xlsx",MediaType.TEXT_PLAIN_VALUE,"1,Prescott,Irene Witt".getBytes());
        mockMvc.perform(MockMvcRequestBuilders.multipart("/projects/batch").file(file))
        .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAllProject()throws Exception {
    
    Project prj=new Project("jdff","jdkijmfdk","dmkid");
    List<Project> listProject=Arrays.asList(prj);
    BDDMockito.given(service.getAllResources(0, 3, "accountManager", "ASC")).willReturn(listProject);
    
    mockMvc.perform( MockMvcRequestBuilders.get("/projects")
    .param("pageNo", "0")
    .param("pageSize", "3")
    .param("sortBy", "accountManager")
    .param("sortAscOrDesc", "ASC")
    .contentType(MediaType.APPLICATION_JSON))
    .andExpect(MockMvcResultMatchers.status().isOk());
    /*.andDo(MockMvcResultHandlers.print())
    .andExpect(MockMvcResultMatchers.jsonPath("[0].id").value(prj.getId()));*/

    
    
    }

    @Test
    public void getProjectById() throws Exception {
        Project project = new Project();
        project.setId("1");
        BDDMockito.given(service.getResourceById(project.getId())).willReturn(project);
        mockMvc.perform(MockMvcRequestBuilders.get("/projects/" + project.getId().toString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(project.getId()));
    }


    @Test
    public void getProjectWithQueryTest() throws Exception{
        Project project = new Project();
        project.setId("1");
        List<Project> projects = new ArrayList<>();
        projects.add(project);
        String queryParam="id:1;";

        BDDMockito.given(service.getQueriedResources(0, 10, "id", "asc",queryParam)).willReturn(projects);
        mockMvc.perform(MockMvcRequestBuilders.get("/projects/query")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.status().isOk());
    }


    @Test
    public void createProject() throws Exception {

        Project project = new Project();
        project.setPmdm("Test Name");
        project.setId("Test Id");

        when(service.createResource(project)).thenReturn(project);
        
        mockMvc.perform(MockMvcRequestBuilders.post("/projects")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJson(project)))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists());
    }

    @Test
    public void testprojectDelete() throws Exception {
        Project project = new Project();
        project.setAccountManager("Test Name");
        project.setPmdm("Test Naeme");
        project.setId("1L");

        doNothing().when(service).deleteResource(project.getId());

        mockMvc.perform(MockMvcRequestBuilders.delete("/projects/" + project.getId().toString())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }
/*
    @Test
    public void deleteException() throws Exception {
        Project project = new Project();
        project.setId("1");
        project.setAccountManager("Test Name");

        Mockito.doThrow(new ProjectNotFound(project.getId())).when(service).deleteProject(project.getId());

        mockMvc.perform(MockMvcRequestBuilders.delete("/projects/" + project.getId().toString())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void getByIdException() throws Exception {
        Project project = new Project();
        project.setId("1L");
        project.setAccountManager("Test Name");

        Mockito.doThrow(new ProjectNotFound(project.getId())).when(service).getProjectbyId(project.getId());

        mockMvc.perform(MockMvcRequestBuilders.get("/projects/" + project.getId().toString())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }


/*


    @Test
    public void testUpdateproject() throws Exception {
        Project project = new Project();
        project.setName("Test Name");
        project.setId("1L");

        BDDMockito.given(service.updateProject(project,project.getId())).willReturn(project);

        ObjectMapper mapper = new ObjectMapper();

        mockMvc.perform(MockMvcRequestBuilders.put("/projects/" + project.getId().toString())
                .content(mapper.writeValueAsString(project))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("name").value(project.getName()));
    }
*/
}