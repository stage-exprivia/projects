package com.exprivia.project.main.batch;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import com.exprivia.commonlibrary.batch.BatchReport;
import com.exprivia.commonlibrary.batch.Report;
import com.exprivia.project.main.batch.Batch;


import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;

public class BatchTest {
    @Test
    public void readExcel() throws IOException {

        MockMultipartFile excel = new MockMultipartFile("risorseTest", new FileInputStream(new File
        ("D:/Users/lessi/Desktop/Progetti lavoro/ProjectsGit/src/test/resources/dataTreCampi.xlsx")));
        Batch batch = new Batch();
        BatchReport batchReport = batch.readExcel(excel);
        System.out.println(batchReport);
        Report report1=new Report(2,"ERROR","Project id must not be empty");
        Report report2=new Report(1,"SUCCESS","Project imported correctly");
        assertEquals((batchReport.getReport().get(1)), report1);
        assertEquals((batchReport.getReport().get(0)), report2);

    }
}
