package com.exprivia.project.main.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.exprivia.commonlibrary.batch.BatchReport;
import com.exprivia.commonlibrary.controllers.BatchControllerInterface;
import com.exprivia.commonlibrary.controllers.ControllerInterface;
import com.exprivia.commonlibrary.exceptions.DateDurationException;
import com.exprivia.commonlibrary.exceptions.ResourceNotFoundException;
import com.exprivia.commonlibrary.models.Project;
import com.exprivia.project.main.service.ProjectService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.media.Content; 
import io.swagger.v3.oas.annotations.media.Schema;

@CrossOrigin("http://localhost:8080")
@RestController
public class ProjectController implements ControllerInterface<Project>, BatchControllerInterface<Project> {


    @Autowired
    ProjectService service;

    //insert batch
    @Operation(summary = "Load an excel file")
    @ApiResponse(responseCode = "200", description = "Upload successful",
    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = BatchReport.class)) })
    @ApiResponse(responseCode = "415", description = "Wrong file, load an '.xlsx' or '.xls' and remember to enter three fields",
    content = @Content)

          @PostMapping(value = "/projects/batch", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity insertListFromExcel (@Parameter (description = "the type of the file must be '.xlsx' or '.xls'") @RequestParam("file") MultipartFile file) { 
		try {
			return new ResponseEntity<>(service.insertResourcesFromExcel(file),HttpStatus.OK);
		} catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.UNSUPPORTED_MEDIA_TYPE);
		}
    }
    //getProject
    @Operation(summary = "Get all project")
    @ApiResponse(responseCode = "200", description = "Get project successful",
    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = BatchReport.class)) })
    @Validated
    @GetMapping("/projects")
    public ResponseEntity <List<Project>> getAllResources(
    @RequestParam(defaultValue = "0") Integer pageNo, 
    @RequestParam (defaultValue="10") @Size (max=100) Integer pageSize,
    @RequestParam(defaultValue = "accountManager") String sortBy,
    @RequestParam(defaultValue = "asc") String sortAscOrDesc)
     {
        List<Project> list = service.getAllResources(pageNo, pageSize, sortBy, sortAscOrDesc);
        return new ResponseEntity<List<Project>>(list, new HttpHeaders(), HttpStatus.OK); 
    }

    //getProjectWithQuery
    @Operation(summary = "Get project by query")
    @ApiResponse(responseCode = "200", description = "Project retrieved",
    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Project.class)) })
    @ApiResponse(responseCode = "404", description = "The query does not return any record", content = @Content)
    @GetMapping(value = "/query")
    public ResponseEntity getQueriedResources(
        @RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
        @RequestParam(name = "pageSize", defaultValue = "10") @Size (max = 100) Integer pageSize,
        @RequestParam(name = "sortField", defaultValue = "cid") String sortField,
        @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir,
     @RequestParam String queryParams) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getQueriedResources(pageNo, pageSize, sortField, sortDir, queryParams));
        } catch (IllegalArgumentException | IllegalAccessException | ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.UNSUPPORTED_MEDIA_TYPE).body(e.getMessage());
        } 
    }
    
    // getProjectById
    @Operation(summary = "Get project by id")
    @ApiResponse(responseCode = "200", description = "Get project by id successful",
    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = BatchReport.class)) })
    @ApiResponse(responseCode = "500", description = "Id not found",
    content = @Content)
    @GetMapping("/projects/{id}")
    public ResponseEntity getResourceById(@PathVariable("id") String id) throws ResourceNotFoundException {
        return new ResponseEntity<Project>(service.getResourceById(id), HttpStatus.OK);
    }

    //postProject
    @Operation(summary = "Insert new project")
    @ApiResponse(responseCode = "201", description = "Insert successful",
    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = BatchReport.class)) })
    @ApiResponse(responseCode = "400", description = "The server cannot or will not process the request due to an apparent client error (e.g., malformed request syntax, size too large, invalid request message framing, or deceptive request routing).",
    content = @Content)
    @PostMapping("/projects")
    public ResponseEntity<Project> insertResource(@RequestBody Project project) {
        return new ResponseEntity<Project>(service.createResource(project), HttpStatus.CREATED);
    }

    // deleteProject
    @Operation(summary = "Delete project")
    @ApiResponse(responseCode = "204", description = "Delete successful",
    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Project.class)) })
    @ApiResponse(responseCode = "500", description = "Id not found",
    content = @Content)
    @DeleteMapping("/projects/{id}")
    //@ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Project> deleteResource(@PathVariable("id") String id) throws ResourceNotFoundException {
        //service.deleteResource(id);
        return new ResponseEntity<Project>(service.deleteResource(id), HttpStatus.OK);
    }

    // updateProject
    @Operation(summary = "Update project")
    @ApiResponse(responseCode = "200", description = "Update successful",
    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Project.class)) })
    @ApiResponse(responseCode = "404", description = "Id not found",
    content = @Content)
    @PutMapping("/projects/{id}")
    public ResponseEntity<Project> updateResource(@Valid Project project, String id)
            throws DateDurationException, ResourceNotFoundException {
        return new ResponseEntity<Project>(service.updateResource(project, id), HttpStatus.OK);
    }







}












