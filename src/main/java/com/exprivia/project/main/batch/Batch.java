package com.exprivia.project.main.batch;

import java.io.IOException;
import java.util.Iterator;

import com.exprivia.commonlibrary.batch.BatchReport;
import com.exprivia.commonlibrary.models.Project;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class Batch {

    public BatchReport readExcel(MultipartFile excel) throws IOException {

        Workbook workbook = WorkbookFactory.create(excel.getInputStream());
        DataFormatter formatter = new DataFormatter();
        BatchReport batchReport=new BatchReport();
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        rowIterator.next();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();

            String idCell = formatter.formatCellValue(row.getCell(0));
            String accountManagerCell = formatter.formatCellValue(row.getCell(1));
            String pm_dmCell = formatter.formatCellValue(row.getCell(2));

            Project project = new Project(idCell, accountManagerCell, pm_dmCell);
            
            if (project.getId().isBlank()) {
                batchReport.addReport(row.getRowNum(),"ERROR","Project id must not be empty");
                batchReport.addNotValidResources(project);
            } else {
                batchReport.addReport(row.getRowNum(),"SUCCESS","Project imported correctly");
                batchReport.addValidResources(project);
            }
        }
        workbook.close();
        return batchReport;
    }
}
