package com.exprivia.project.main.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.exprivia.commonlibrary.batch.BatchReport;
import com.exprivia.commonlibrary.exceptions.ResourceNotFoundException;
import com.exprivia.commonlibrary.filter.QueryBuilder;
import com.exprivia.commonlibrary.models.Project;
import com.exprivia.commonlibrary.repository.ProjectRepository;
import com.exprivia.commonlibrary.service.BatchServiceInterface;
import com.exprivia.commonlibrary.service.ServiceInterface;
import com.exprivia.project.main.batch.Batch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;



@Service
//@Transactional
public class ProjectService implements ServiceInterface<Project, String>,BatchServiceInterface<BatchReport> {
    
    @Autowired
    ProjectRepository projectRepository;

    Pageable paging;

    Batch readFile=new Batch();

    @Autowired
    QueryBuilder<Project> queryBuilder;

    @Autowired
    MongoTemplate mongoTemplate;

    public BatchReport insertResourcesFromExcel(MultipartFile excel) throws IOException{
        BatchReport batchReport;
        batchReport = readFile.readExcel(excel);
        projectRepository.saveAll(batchReport.getValidResources());
        return batchReport;
    }

    public List<Project> getAllResources(Integer pageNumber, Integer pageSize, String sortBy, String sort){
        if (sort.equalsIgnoreCase("asc")){
            paging= PageRequest.of(pageNumber, pageSize, Sort.by(Direction.ASC, sortBy));
        }
        else if (sort.equalsIgnoreCase("desc")){
            paging= PageRequest.of(pageNumber, pageSize, Sort.by(Direction.DESC, sortBy));
        }
        else{
            paging= PageRequest.of(pageNumber, pageSize);
        }
        Page<Project> pagedResult = projectRepository.findAll(paging);
        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<Project>();
        }
    }

    public List<Project> getQueriedResources(Integer pageNum, Integer pageSize, String sortField, String sortDir, String queryParams) throws ResourceNotFoundException, IllegalArgumentException, IllegalAccessException {
        Pageable paging = PageRequest.of(pageNum, pageSize, 
        sortDir.equals("desc") ? Sort.by(sortField).descending() : Sort.by(sortField).ascending());

        Query query = queryBuilder.buildQuery(queryParams, Project.class).with(paging);
        List<Project> queriedResult = mongoTemplate.find(query, Project.class);
        if(queriedResult.isEmpty()) {
            throw new ResourceNotFoundException("Project Not Found");
        } else {
            return queriedResult;
        }
    }


    public Project getResourceById(String id) throws ResourceNotFoundException {
        Optional<Project> newProject = projectRepository.findById(id);
        if (!newProject.isPresent()) {
            throw new ResourceNotFoundException(id);
        } else {
            return newProject.get();
        }
    }

    public Project createResource(Project project) {
        return projectRepository.save(project);
    }

    public Project deleteResource(String id) throws ResourceNotFoundException {
        Optional<Project> newProject = projectRepository.findById(id);
        if (!newProject.isPresent()) {
            throw new ResourceNotFoundException(id);
        } else {
            projectRepository.deleteById(id);
        }
        return newProject.get();
    }

    public Project updateResource(Project project, String id) throws ResourceNotFoundException {
        Optional<Project> newProject = projectRepository.findById(id);
        if (!newProject.isPresent()) {
            throw new ResourceNotFoundException(id);
        } else {
            project.setId(id);
            return projectRepository.save(project);
        }
    }





   


/*
    public Project updateProject(Project project, String id) throws ProjectNotFound {
        Optional<Project> newProject = projectRepository.findById(project.getId());
        if (!newProject.isPresent()) {
            throw new ProjectNotFound(id);
        } else {
            project.setId(id);
            return projectRepository.save(project);
        }
    }





    
    */
}
